"""MQTT-Telnet mapper."""
import logging
import telnetlib
import time

import click
import paho.mqtt.client as mqtt

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(levelname)s] - %(asctime)s - %(name)s: %(message)s"
)
log = logging.getLogger('telnethandler')
TELNET_PORT = 23
TIMEOUT = 10


# pylint: disable=unused-argument
def on_message(client, userdata, message):
    """On MQTT msg."""
    log.debug("Topic: %s\tMsg: %s", message.topic, message.payload)
    try:
        ip_address, msg = str(message.payload).split(':', 1)
        log.info("IP: %s\tMSG: %s", ip_address, msg)
        with telnetlib.Telnet(ip_address, TELNET_PORT, TIMEOUT) as session:
            session.write(f"{message.payload}\n".encode())
    except BaseException as error:
        client.publish("telnet/error", str(error))


@click.command()
@click.option('--mqtt_host', default='localhost', help='Host of MQTT Broker.')
def run(mqtt_host):
    """Execute telnet-handler."""
    client = mqtt.Client("telnet-handler")
    client.connect(mqtt_host)
    client.loop_start()
    client.subscribe("telnet/send")
    client.on_message = on_message
    while True:
        time.sleep(1)


if __name__ == '__main__':
    run('localhost')
