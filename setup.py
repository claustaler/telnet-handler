"""Setup file for python_template."""
import os
from typing import List

from setuptools import find_namespace_packages, setup

VERSION = '1.0.0'
REQUIREMENTS: List[str] = ['paho-mqtt', 'telnetlib2', 'click']
PACKAGES = find_namespace_packages(include=['telnethandler*'])


def _md_doc():
    with open(os.path.dirname(os.path.abspath(__file__)) + '/README.md') as file:
        return file.read()


setup(
    name='telnethandler',
    version=VERSION,
    description='Package to Handle telnet msg.',
    author='claus.scharfenberg',
    author_email='it@scharfenberg.eu',
    url='git@gitlab.com:claustaler/telnet-handler.git',
    include_package_data=True,
    packages=PACKAGES,
    install_requires=REQUIREMENTS,
    command_options={'build_sphinx': {'source_dir': ('setup.py', 'source')}},
    entry_points={'console_scripts': ['telnethandler=telnethandler:run']},
    zip_safe=True,
    long_description=_md_doc(),
    long_description_content_type='text/markdown'
)
